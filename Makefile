.PHONY: run

permissions = --unstable --allow-read --allow-write --allow-run

install:
	deno install -f $(permissions) -n dake src/main.ts
