import { colorize } from "https://deno.land/x/ink@1.3/mod.ts";

export class BugError extends Error {
    printed = false;

    constructor(
        // deno-lint-ignore no-explicit-any
        public cause: any
    ) {
        super();
    }

    toString() {
        return this.cause.toString();
    }
}

export const printBug = (bug: BugError, force=false) => {
    if (bug.printed && !force) return;
    bug.printed = true;

    const gitlabBugIssueUrl = `https://gitlab.com/x3ro/dake/-/issues/new?issuable_template=BUG&issue[title]=[BUG] ${bug}`;
    const gitlabBugIssueLink = `${gitlabBugIssueUrl}`.replaceAll(" ", "%20");

    console.error(
        colorize(
            `<red><b>[BUG]</b></red> You discovered a bug! 🐞 This is NOT your fault.\n` +
            `      Please report it over at GitLab: <i>${gitlabBugIssueLink}<i>\n` +
            `      <yellow>Error: <gray>${bug}</gray>`
        )
    );
};

export const BUG = (
    // deno-lint-ignore no-explicit-any
    cause: any
) => {
    const bug = new BugError(cause);
    printBug(bug);
    return bug;
};
