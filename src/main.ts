import { Parser } from "./parser.ts";
import { cleanup } from "./exit.ts";
import { makeTempFile } from "./temp.ts";
import { BugError, printBug } from "./BugError.ts";

const main = async () => {
    const tmpMakefile = await makeTempFile({ prefix: "dake-makefile." });

    const dakefile = await tryOpenDakefile();
    const makefile = await Deno.open(tmpMakefile, { write: true });

    const parser = new Parser(dakefile);
    await parser.output(makefile).parse();

    makefile.close();

    let args = Deno.args;
    let passArgs: string[] = [];
    const passArgsPos = args.indexOf(":");
    if (passArgsPos >= 0) {
        passArgs = args.slice(passArgsPos + 1);
        args = args.slice(0, passArgsPos);
    }
    const passArgsStr = passArgs
        .map((x) => x.replaceAll("\\", "\\\\"))
        .map((x) => x.replaceAll("$", "\\$$$$"))
        .map((x) => x.replaceAll('"', '\\"'))
        .map((x) => `"${x}"`)
        .join(" ");

    const cmd = [
        "make",
        `--makefile=${tmpMakefile}`,
        `ARGS=${passArgsStr}`,
        ...args,
    ];
    const env = {
        MAKE: "dake",
        DAKE: "dake",
    };
    await Deno.run({ cmd, env }).status();
};

const tryOpenDakefile = async (): Promise<Deno.File> => {
    const dakefile = "Dakefile";
    try {
        return await Deno.open(dakefile);
    } catch (ex) {
        if (!(ex instanceof Deno.errors.NotFound)) {
            throw ex;
        }

        console.error(
            `ERROR: There is no ${dakefile} in the current directory :(`
        );

        Deno.exit(1);
    }
};

try {
    await main();
} catch (ex) {
    cleanup();

    if (ex instanceof BugError) {
        printBug(ex);
        throw ex.cause;
    }

    throw ex;
}
