import { readLines } from "https://deno.land/std@0.92.0/io/bufio.ts";
import { BUG } from "./BugError.ts";

interface Action {
    name: string;
    comment: string;
}

export class Parser {
    // TODO: Use streams instead of `Deno.File`
    private outputFile?: Deno.File;
    private comment = "";
    private actions: Action[] = [];
    private extraPhonies: string[] = [];
    private encoder: TextEncoder;

    constructor(private file: Deno.File) {
        this.encoder = new TextEncoder();
    }

    output(output: Deno.File) {
        this.outputFile = output;
        return this;
    }

    async parse() {
        this.extraPhonies.push("__dake_default_target__");
        await this.writeLn("__dake_default_target__: __dake_list_actions__");
        await this.writeLn("");

        // TODO: Move this somewhere else
        await this.writeLn("unexport ARGS");

        let idx = 0;
        for await (const line of readLines(this.file)) {
            idx++;
            try {
                await this.processLine(line);
            } catch (e) {
                // Print to stdout and exit
                console.error(`Encountered error in line ${idx}: ${e}`);
            }
        }

        await this.generateHelp();
        await this.generatePhony();
    }

    private isDescriptionLine(line: string) {
        return line.startsWith("### ");
    }

    private isCommentLine(line: string) {
        return line.startsWith("#");
    }

    private isEscapedLine(line: string) {
        return line.startsWith("\\");
    }

    private isActionLine(line: string) {
        return line.startsWith("% ");
    }

    private async processLine(line: string) {
        if (this.isDescriptionLine(line)) {
            this.comment = line.substr(4);
            await this.writeLn(line);
            return;
        }

        if (this.isCommentLine(line)) {
            await this.writeLn(line);
            return;
        }

        if (this.isEscapedLine(line)) {
            this.comment = "";
            await this.writeLn(line.substr(1));
            return;
        }

        if (!this.isActionLine(line)) {
            this.comment = "";
            await this.writeLn(line);
            return;
        }

        const matches = line.match(/% (?<action>.*(?=[^\\]:).)/);
        // TODO: can this happen?
        if (matches === null) {
            this.comment = "";
            await this.writeLn(line);
            return;
        }

        if (matches.groups == undefined) throw BUG("Regex match has no groups");
        if (matches.groups["action"] == undefined)
            throw BUG("Regex group `action` does not exist");

        line = line.substr(2);
        await this.writeLn(line);

        const name = matches.groups["action"];

        this.actions.push({
            name,
            comment: this.comment,
        });
    }

    private async generatePhony() {
        const phony = this.actions
            .map((x) => x.name)
            .concat(this.extraPhonies)
            .join(" ");
        await this.writeLn("");
        await this.writeLn(`.PHONY: ${phony}`);
    }

    private async generateHelp() {
        const addDeprecatedListAlias = async (cmd: string) => {
            this.extraPhonies.push(cmd);
            await this.writeLn(`${cmd}: __dake_list_actions__`);
            await this.writeLn("\t@echo >&2 ''");
            await this.writeLn(
                `\t@echo >&2 'WARNING: Using \`${cmd}\` to list actions is deprecated. Execute \`dake\` without parameters to list actions.'`
            );
        };
        const pad = 24;

        await this.writeLn("");

        await addDeprecatedListAlias("\\:");
        await addDeprecatedListAlias("\\:list");
        await addDeprecatedListAlias("-l");
        await addDeprecatedListAlias("--list");

        this.extraPhonies.push("__dake_list_actions__");
        await this.writeLn("__dake_list_actions__:");
        await this.writeLn(`\t@echo ""`);
        await this.writeLn(`\t@echo "The following actions are awailable:"`);
        await this.writeLn('\t@echo ""');

        for (const { name, comment } of this.actions) {
            let sep = "\n";
            if (name.length < pad - 3) {
                sep = "".padEnd(pad - name.length);
            }

            await this.writeLn(`\t@echo '    ${name}${sep}${comment}'`);
        }
    }

    private async writeLn(line: string) {
        await this.outputFile?.write(this.encoder.encode(line + "\n"));
    }
}
