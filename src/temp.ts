import { onExit, once } from "./exit.ts";

export const makeTempFile = async (
    options?: Deno.MakeTempOptions | undefined
): Promise<string> => {
    const tmp = await Deno.makeTempFile(options);
    onExit(
        once(async () => {
            await Deno.remove(tmp);
        })
    );
    return tmp;
};
