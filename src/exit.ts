type ExitEventHandler = () => void;

const exitHandlers: ExitEventHandler[] = [];

const setup = () => {
    self.addEventListener("unload", cleanup);
    Deno.addSignalListener("SIGINT", cleanup);
    Deno.addSignalListener("SIGTERM", cleanup);
};

export const onExit = (listener: ExitEventHandler) => {
    exitHandlers.push(listener);
};

export const once = (callback: () => void) => {
    let triggered = false;
    return () => {
        if (triggered) return;
        triggered = true;
        callback();
    };
};

export const cleanup = () => {
    exitHandlers.forEach((handler) => handler());
};

setup();
